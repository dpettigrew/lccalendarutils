//
//  LCCalendarUtils.h
//
//  Created by David Pettigrew on 3/3/13.
//  Copyright (c) 2013 LifeCentrics, LLC.
//  This code is distributed under the terms and conditions of the MIT license.
//
//  Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

//  The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

//  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

#import <Foundation/Foundation.h>

enum TimeDay { eNotSet=0, eSunday, eMonday, eTuesday, eWednesday, eThursday, eFriday, eSaturday, eWeekDays } TimeDay;

@interface LCCalendarUtils : NSObject

+ (NSDate *)midnightForDate:(NSDate *)date withOffset:(NSInteger)daysOffset;
+ (NSDate *)getNextMidnightAfterDate:(NSDate *)date;
// 0 weeksOffset means this week, 1 weeksOffset is not this Sunday but next etc
+ (NSDate *)endOfWeekForDate:(NSDate *)date withOffset:(NSInteger)weeksOffset;

// returns the date of the midnight of the previous Monday from the date. i.e the start of the week
+ (NSDate *)startOfWeekForDate:(NSDate *)date;
// returns 11:59:59pm on the following Sunday from the date
+ (NSDate *)endOfWeekForDate:(NSDate *)date;
/**
 Test if the time is the same as the current day correcting for the time zone.
 som 23:59 remains 23:59
 */
+ (Boolean)isToday:(NSDateComponents *)comps;

// These offset and set to 23:59:59
+ (NSDate *)endOfHourForDate:(NSDate *)date withOffset:(NSInteger)hoursOffset;
// 1 second before end of month
+ (NSDate *)endOfMonth:(NSDate *)date withOffset:(NSInteger)monthsOffset;
+ (NSDate *)startOfMonth:(NSDate *)date withOffset:(NSInteger)monthsOffset;
+ (NSDate *)startOfYearForDate:(NSDate *)date withOffset:(NSInteger)yearsOffset;
+ (NSDate *)endOfYearForDate:(NSDate *)date withOffset:(NSInteger)yearsOffset;
+ (NSDate *)firstWeekDay:(NSInteger)weekday fromDate:(NSDate *)startDate;
+ (NSDate *)midnightOfNextWeekdayFromDate:(NSDate *)date;

+ (NSDate *)date:(NSDate *)date offsetBySeconds:(NSInteger)secondsOffset;
+ (NSDate *)date:(NSDate *)date offsetByHours:(NSInteger)hoursOffset;
+ (NSDate *)date:(NSDate *)date offsetByDays:(NSInteger)daysOffset;
+ (NSDate *)date:(NSDate *)date offseetByWeeks:(NSInteger)weeksOffset;
+ (NSDate *)date:(NSDate *)date offsetByMonths:(NSInteger)monthsOffset;
+ (NSDate *)date:(NSDate *)date offsetByYears:(NSInteger)monthsOffset;

+ (NSDate *)nextWeekday:(NSInteger)weekday afterDate:(NSDate *)startDate;
+ (NSDate *)nextWeekdayAfterDate:(NSDate *)date;

+ (NSDate *)startOfQuarterFromDate:(NSDate *)date;
+ (NSDate *)endOfQuarterFromDate:(NSDate *)date;

@end
