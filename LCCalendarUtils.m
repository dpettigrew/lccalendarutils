//
//  LCCalendarUtils.m
//
//  Created by David Pettigrew on 3/3/13.
//  Copyright (c) 2013 LifeCentrics, LLC.
//  This code is distributed under the terms and conditions of the MIT license.
//
//  Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

//  The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

//  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//
//

#import "LCCalendarUtils.h"

@implementation LCCalendarUtils

static NSCalendar *gregorian = nil;

+ (void)initialize
{
    gregorian = [[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar];
}

+ (NSDate *)midnightForDate:(NSDate *)date withOffset:(NSInteger)daysOffset {
	unsigned unitFlags = NSYearCalendarUnit | NSMonthCalendarUnit | NSDayCalendarUnit | NSHourCalendarUnit | NSMinuteCalendarUnit | NSSecondCalendarUnit | NSWeekdayCalendarUnit;
    NSCalendar *cal = [NSCalendar currentCalendar];
	NSDateComponents *dateComponents = [cal components:unitFlags fromDate:date];
	[dateComponents setSecond:00];
	[dateComponents setMinute:00];
	[dateComponents setHour:00];
	NSDate *midnightDate = [cal dateFromComponents:dateComponents];
	NSDateComponents *offsetComponents = [[NSDateComponents alloc] init];
	[offsetComponents setDay:daysOffset];
	NSDate *nextDeadline = [cal dateByAddingComponents:offsetComponents toDate:midnightDate options:0];
	return nextDeadline;
}

+ (NSDate *)getNextMidnightAfterDate:(NSDate *)date {
	return [self midnightForDate:date withOffset:1];
}

// 0 weeksOffset means this week, 1 weeksOffset is not this Sunday but next etc
+ (NSDate *)endOfWeekForDate:(NSDate *)date withOffset:(NSInteger)weeksOffset {
	NSDate *nextDeadline = [self getNextMidnightAfterDate:date];
    NSCalendar *cal = [NSCalendar currentCalendar];
	NSDateComponents *dateComponents = [cal components:NSWeekdayCalendarUnit fromDate:date];
	NSInteger weekday = [dateComponents weekday];
    
	NSInteger dayOffset = eSunday - weekday; // compute days to offset to get to Sunday
	if (dayOffset < 0) { // weekday is earlier in week, add a week to offsetComponents
		weeksOffset++;
	}
    
	NSDateComponents *offsetComponents = [[NSDateComponents alloc] init];
	[offsetComponents setWeekOfYear:weeksOffset];
	[offsetComponents setDay:dayOffset];
	nextDeadline = [cal dateByAddingComponents:offsetComponents toDate:nextDeadline options:0];
	return nextDeadline;
}


+ (Boolean)isToday:(NSDateComponents *)comps
{
	NSCalendar *gregorian = [[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar];
	NSDateComponents *todayComps = [gregorian components:NSDayCalendarUnit|NSMonthCalendarUnit|NSYearCalendarUnit fromDate:[NSDate date]];
	if ([comps year] == [todayComps year] && [comps month] == [todayComps month] && [comps day] == [todayComps day])
		return YES;
	return NO;
}

+ (NSDate *)endOfHourForDate:(NSDate *)date withOffset:(NSInteger)hoursOffset {
	unsigned unitFlags = NSYearCalendarUnit | NSMonthCalendarUnit | NSDayCalendarUnit | NSHourCalendarUnit | NSMinuteCalendarUnit | NSSecondCalendarUnit | NSWeekdayCalendarUnit;
	NSDateComponents *dateComponents = [gregorian components:unitFlags fromDate:date];
	[dateComponents setSecond:00];
	[dateComponents setMinute:00];
	NSDate *endOfHourDate = [gregorian dateFromComponents:dateComponents];
    
	NSDateComponents *offsetComponents = [[NSDateComponents alloc] init];
	[offsetComponents setHour:hoursOffset];
	NSDate *nextDeadline = [gregorian dateByAddingComponents:offsetComponents toDate:endOfHourDate options:0];
	return nextDeadline;
}

+ (NSDate *)firstWeekDay:(NSInteger)weekday fromDate:(NSDate *)startDate {
	NSDate *nextDeadline = [self getNextMidnightAfterDate:startDate];
	NSDateComponents *dateComponents = [gregorian components:NSWeekdayCalendarUnit fromDate:startDate];
	NSInteger origWeekday = [dateComponents weekday];
	NSInteger weekOffset = 0;
	NSInteger dayOffset = weekday - origWeekday;
	if (dayOffset <= 0) {
		weekOffset++;
	}
    
	NSDateComponents *offsetComponents = [[NSDateComponents alloc] init];
	[offsetComponents setWeekOfYear:weekOffset];
	[offsetComponents setDay:dayOffset];
	nextDeadline = [gregorian dateByAddingComponents:offsetComponents toDate:nextDeadline options:0];
	return nextDeadline;
}

+ (NSDate *)nextWeekday:(NSInteger)weekday afterDate:(NSDate *)startDate {
	NSDateComponents *dateComponents = [gregorian components:NSWeekdayCalendarUnit fromDate:startDate];
	NSInteger origWeekday = [dateComponents weekday];
	NSInteger weekOffset = 0;
	NSInteger dayOffset = weekday - origWeekday;
	if (dayOffset <= 0) {
		weekOffset++;
	}
    
	NSDateComponents *offsetComponents = [[NSDateComponents alloc] init];
	[offsetComponents setWeekOfYear:weekOffset];
	[offsetComponents setDay:dayOffset];
	NSDate *nextDeadline = [gregorian dateByAddingComponents:offsetComponents toDate:startDate options:0];
	return nextDeadline;
}

+ (NSDate *)startOfMonth:(NSDate *)date withOffset:(NSInteger)monthsOffset {
	unsigned unitFlags =  NSYearCalendarUnit | NSMonthCalendarUnit | NSDayCalendarUnit | NSHourCalendarUnit | NSMinuteCalendarUnit | NSSecondCalendarUnit ;
	NSDateComponents *dateComponents = [gregorian components:unitFlags fromDate:date];
	NSInteger newMonth = ([dateComponents month] + monthsOffset)%12;
	NSInteger newYear = [dateComponents year] + ([dateComponents month] + monthsOffset)/12;
	[dateComponents setSecond:00];
	[dateComponents setMinute:00];
	[dateComponents setHour:00];
	[dateComponents setMonth:newMonth];
	[dateComponents setYear:newYear];
	[dateComponents setDay:1];
	NSDate *nextDeadline = [gregorian dateFromComponents:dateComponents];
	return nextDeadline;
}

+ (NSDate *)endOfMonth:(NSDate *)date withOffset:(NSInteger)monthsOffset {
    NSDate *startOfProceedingMonth = [self startOfMonth:date withOffset:monthsOffset + 1];
    NSDate *endOfMonth = [self date:startOfProceedingMonth offsetBySeconds:-1];
    return endOfMonth;
}

+ (NSDate *)endOfYearForDate:(NSDate *)date withOffset:(NSInteger)yearsOffset {
	unsigned unitFlags =  NSYearCalendarUnit | NSMonthCalendarUnit | NSDayCalendarUnit | NSHourCalendarUnit | NSMinuteCalendarUnit | NSSecondCalendarUnit ;
	NSDateComponents *dateComponents = [gregorian components:unitFlags fromDate:date];
	
	[dateComponents setSecond:59];
	[dateComponents setMinute:59];
	[dateComponents setHour:23];
	[dateComponents setMonth:12];
	[dateComponents setDay:31];
	[dateComponents setYear:[dateComponents year] + yearsOffset];
	NSDate *nextDeadline = [gregorian dateFromComponents:dateComponents]; // updated to end of current year
	return nextDeadline;
}

+ (NSDate *)startOfYearForDate:(NSDate *)date withOffset:(NSInteger)yearsOffset {
	unsigned unitFlags =  NSYearCalendarUnit | NSMonthCalendarUnit | NSDayCalendarUnit | NSHourCalendarUnit | NSMinuteCalendarUnit | NSSecondCalendarUnit ;
	NSDateComponents *dateComponents = [gregorian components:unitFlags fromDate:date];
	
	[dateComponents setSecond:0];
	[dateComponents setMinute:0];
	[dateComponents setHour:0];
	[dateComponents setMonth:1];
	[dateComponents setDay:1];
	[dateComponents setYear:[dateComponents year] + yearsOffset];
	NSDate *nextDeadline = [gregorian dateFromComponents:dateComponents]; // updated to end of current year
	return nextDeadline;
}

+ (NSDate *)date:(NSDate *)date offsetBySeconds:(NSInteger)secondsOffset {
	NSDateComponents *offsetComponents = [[NSDateComponents alloc] init];
    [offsetComponents setSecond:secondsOffset];
	NSDate *offsetDate = [gregorian dateByAddingComponents:offsetComponents toDate:date options:0];
	return offsetDate;
}

+ (NSDate *)date:(NSDate *)date offsetByHours:(NSInteger)hoursOffset {
	NSDateComponents *offsetComponents = [[NSDateComponents alloc] init];
	[offsetComponents setHour:hoursOffset];
	NSDate *nextDeadline = [gregorian dateByAddingComponents:offsetComponents toDate:date options:0];
	return nextDeadline;
}

+ (NSDate *)date:(NSDate *)date offsetByDays:(NSInteger)daysOffset {
	NSDateComponents *offsetComponents = [[NSDateComponents alloc] init];
	[offsetComponents setDay:daysOffset];
	NSDate *nextDeadline = [gregorian dateByAddingComponents:offsetComponents toDate:date options:0];
	return nextDeadline;
}

+ (NSDate *)date:(NSDate *)date offseetByWeeks:(NSInteger)weeksOffset {
	NSDateComponents *offsetComponents = [[NSDateComponents alloc] init];
	[offsetComponents setWeekOfYear:weeksOffset];
	NSDate *nextDeadline = [gregorian dateByAddingComponents:offsetComponents toDate:date options:0];
	return nextDeadline;
}

// one month from date
+ (NSDate *)date:(NSDate *)date offsetByMonths:(NSInteger)monthsOffset {
	NSDateComponents *offsetComponents = [[NSDateComponents alloc] init];
	[offsetComponents setMonth:monthsOffset];
	NSDate *nextDeadline = [gregorian dateByAddingComponents:offsetComponents toDate:date options:0];
	return nextDeadline;
}

+ (NSDate *)date:(NSDate *)date offsetByYears:(NSInteger)yearsOffset {
	NSDateComponents *offsetComponents = [[NSDateComponents alloc] init];
	[offsetComponents setYear:yearsOffset];
	NSDate *nextDeadline = [gregorian dateByAddingComponents:offsetComponents toDate:date options:0];
	return nextDeadline;
}

+ (NSDate *)midnightOfNextWeekdayFromDate:(NSDate *)date {
	NSDate *nextDeadline = [self getNextMidnightAfterDate:date];
	NSDateComponents *dateComponents = [gregorian components:NSWeekdayCalendarUnit fromDate:date];
	NSInteger weekday = [dateComponents weekday];
    NSInteger dayAdvance = 0;
	if (weekday == eSaturday) {
		dayAdvance = 2;
	}
	else if	(weekday == eFriday) {
		dayAdvance = 3;
	}
	else {
		dayAdvance = 1;
	}
    
	NSDateComponents *offsetComponents = [[NSDateComponents alloc] init];
    [offsetComponents setDay:dayAdvance];
	nextDeadline = [gregorian dateByAddingComponents:offsetComponents toDate:nextDeadline options:0];
	return nextDeadline;
}

+ (NSDate *)nextWeekdayAfterDate:(NSDate *)date {
	NSDateComponents *dateComponents = [gregorian components:NSWeekdayCalendarUnit fromDate:date];
	NSInteger weekday = [dateComponents weekday];
    NSInteger dayAdvance = 0;
	if (weekday == eSaturday) {
		dayAdvance = 2;
	}
	else if	(weekday == eFriday) {
		dayAdvance = 3;
	}
	else {
		dayAdvance = 1;
	}
    
	NSDateComponents *offsetComponents = [[NSDateComponents alloc] init];
    [offsetComponents setDay:dayAdvance];
	NSDate *nextDeadline = [gregorian dateByAddingComponents:offsetComponents toDate:date options:0];
	return nextDeadline;
}

+ (NSDate *)startOfWeekForDate:(NSDate *)date {
    
	NSDateComponents *dateComponents = [gregorian components:NSWeekdayCalendarUnit fromDate:date];
	NSInteger weekday = [dateComponents weekday];
    NSInteger dayOffset = 0;
    switch (weekday) {
        case eSunday:
            dayOffset = -6;
            break;
        case eMonday:
            dayOffset = 0;
            break;
        case eTuesday:
            dayOffset = -1;
            break;
        case eWednesday:
            dayOffset = -2;
            break;
        case eThursday:
            dayOffset = -3;
            break;
        case eFriday:
            dayOffset = -4;
            break;
        case eSaturday:
            dayOffset = -5;
            break;
            
        default:
            break;
    }    
	NSDateComponents *offsetComponents = [[NSDateComponents alloc] init];
    [offsetComponents setDay:dayOffset];
	NSDate *mondayDate = [gregorian dateByAddingComponents:offsetComponents toDate:date options:0];
    
	return [self midnightForDate:mondayDate withOffset:0];
}

+ (NSDate *)endOfWeekForDate:(NSDate *)date {
    
	NSDateComponents *dateComponents = [gregorian components:NSWeekdayCalendarUnit fromDate:date];
	NSInteger weekday = [dateComponents weekday];
    NSInteger dayOffset = 0;
    switch (weekday) {
        case eSunday:
            dayOffset = 0;
            break;
        case eMonday:
            dayOffset = 6;
            break;
        case eTuesday:
            dayOffset = 5;
            break;
        case eWednesday:
            dayOffset = 4;
            break;
        case eThursday:
            dayOffset = 3;
            break;
        case eFriday:
            dayOffset = 2;
            break;
        case eSaturday:
            dayOffset = 1;
            break;
            
        default:
            break;
    }
	NSDateComponents *offsetComponents = [[NSDateComponents alloc] init];
    [offsetComponents setDay:dayOffset];
	NSDate *sundayDate = [gregorian dateByAddingComponents:offsetComponents toDate:date options:0];

    unsigned unitFlags =  NSYearCalendarUnit | NSMonthCalendarUnit | NSDayCalendarUnit | NSHourCalendarUnit | NSMinuteCalendarUnit | NSSecondCalendarUnit ;
	NSDateComponents *sundayDateComponents = [gregorian components:unitFlags fromDate:sundayDate];
	
	[sundayDateComponents setSecond:59];
	[sundayDateComponents setMinute:59];
	[sundayDateComponents setHour:23];
	return [gregorian dateFromComponents:sundayDateComponents]; 
}

//+ (NSDate *)dateByOffsettingMonths:(NSInteger)months fromDate:(NSDate *)date {
//	NSDateComponents *offsetComponents = [[NSDateComponents alloc] init];
//    [offsetComponents setMonth:months];
//	NSDate *offsetDate = [gregorian dateByAddingComponents:offsetComponents toDate:date options:0];
//	return offsetDate;
//}
//
//+ (NSDate *)dateByOffsettingDays:(NSInteger)days fromDate:(NSDate *)date {
//	NSDateComponents *offsetComponents = [[NSDateComponents alloc] init];
//    [offsetComponents setDay:days];
//	NSDate *offsetDate = [gregorian dateByAddingComponents:offsetComponents toDate:date options:0];
//	return offsetDate;
//}


+ (NSDate *)startOfQuarterFromDate:(NSDate *)date {
    unsigned unitFlags =  NSYearCalendarUnit | NSMonthCalendarUnit | NSDayCalendarUnit | NSHourCalendarUnit | NSMinuteCalendarUnit | NSSecondCalendarUnit ;
	NSDateComponents *dateComponents = [gregorian components:unitFlags fromDate:date];
    if (dateComponents.month > 1 && dateComponents.month <= 3) {
        dateComponents.month = 1;
    }
    else if (dateComponents.month > 4 && dateComponents.month <= 6) {
        dateComponents.month = 4;
    }
    else if (dateComponents.month > 7 && dateComponents.month <= 9) {
        dateComponents.month = 7;
    }
    else if (dateComponents.month > 10 && dateComponents.month <= 12) {
        dateComponents.month = 10;
    }
	NSDate *intermedDate = [gregorian dateFromComponents:dateComponents];
    return [self startOfMonth:intermedDate withOffset:0];
}

+ (NSDate *)endOfQuarterFromDate:(NSDate *)date {
    unsigned unitFlags =  NSYearCalendarUnit | NSMonthCalendarUnit | NSDayCalendarUnit | NSHourCalendarUnit | NSMinuteCalendarUnit | NSSecondCalendarUnit ;
	NSDateComponents *dateComponents = [gregorian components:unitFlags fromDate:date];
    if (dateComponents.month >= 1 && dateComponents.month < 3) {
        dateComponents.month = 3;
    }
    else if (dateComponents.month >= 4 && dateComponents.month < 6) {
        dateComponents.month = 6;
    }
    else if (dateComponents.month >= 7 && dateComponents.month < 9) {
        dateComponents.month = 9;
    }
    else if (dateComponents.month >= 10 && dateComponents.month < 12) {
        dateComponents.month = 12;
    }
	NSDate *intermedDate = [gregorian dateFromComponents:dateComponents];
    return [self endOfMonth:intermedDate withOffset:0];
}

@end
